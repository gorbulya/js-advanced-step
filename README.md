# js-advanced-step

### Ilia
- Modal class
- Form, FormButton classes
- VisitCardiologist, VisitTherapist, VisitCardiologist classes
- Functions AJAXRequest, createNewVisit, getDataFromCard, showNoVisitText
### Sergey
- FormInput, FormSelect, FormTextarea classes
- Visit class
### Artem
- Bootstrap markup
- Cookies (set/get)
- Cards filter

### Collaboration
- app.js
- Classes Form, Visit. Constants.js module
- gitignore
- css file
- architecture scheme image

![architecture scheme](./assets/ArchitectureScheme.png)